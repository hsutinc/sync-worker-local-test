package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	ginUtil "gitlab.migoinc.com/migotv/golang/gin/util"
)

var Route ginUtil.Info = ginUtil.Info{
	Name: "test",
	Endpoints: []ginUtil.Endpoint{
		{
			Method:   http.MethodGet,
			Version:  1,
			Callback: showID,
			Path:     ":file_id/blocks",
		},
	},
}

var SyncWorkerRoute ginUtil.Info = ginUtil.Info{
	Name: "simplex",
	Endpoints: []ginUtil.Endpoint{
		{
			Method:   http.MethodGet,
			Version:  1,
			Callback: getStatus,
			Path:     "mds-status",
		},
	},
}

func showID(c *gin.Context) {
	id := c.Param("file_id")
	c.String(http.StatusOK, "Gin Api Test with ID: %s", id)
}

type MDSStatus struct {
	CID               string `json:"cid"`
	MID               string `json:"mid"`
	IPAddress         string `json:"ip_address"`
	HasDish           bool   `json:"has_dish"`
	IsSatelliteLocked bool   `json:"is_satellite_locked"`
}

func getStatus(c *gin.Context) {
	c.JSON(http.StatusOK, []MDSStatus{
		{
			CID:               "12",
			MID:               "SA1",
			IPAddress:         "10.10.10.10",
			HasDish:           false,
			IsSatelliteLocked: false,
		},
		{
			CID:               "23",
			MID:               "SA2",
			IPAddress:         "20.20.20.20",
			HasDish:           true,
			IsSatelliteLocked: true,
		},
		{
			CID:               "34",
			MID:               "SA3",
			IPAddress:         "30.30.30.30",
			HasDish:           false,
			IsSatelliteLocked: true,
		},
		{
			CID:               "4",
			MID:               "SC9004",
			IPAddress:         "4.4.4.4",
			HasDish:           true,
			IsSatelliteLocked: false,
		},
		{
			CID:               "5",
			MID:               "SC9005",
			IPAddress:         "5.5.5.5",
			HasDish:           true,
			IsSatelliteLocked: true,
		},
		{
			CID:               "6",
			MID:               "SC9006",
			IPAddress:         "6.6.6.6",
			HasDish:           false,
			IsSatelliteLocked: true,
		},
	})
}

var routes []ginUtil.Info = []ginUtil.Info{
	Route,
	SyncWorkerRoute,
}

func main() {
	router := gin.Default()
	ginUtil.SetRoutes(router, routes)
	srv := &http.Server{
		Addr:    ":4002",
		Handler: router,
	}

	log.Fatal(srv.ListenAndServe())
}
